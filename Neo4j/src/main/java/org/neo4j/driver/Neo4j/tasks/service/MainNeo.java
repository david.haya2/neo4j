package org.neo4j.driver.Neo4j.tasks.service;
import org.neo4j.driver.*;


import static org.neo4j.driver.Values.parameters;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;



public class MainNeo extends JFrame
{
    // Los drivers son seguros para subprocesos y, por lo general, est�n disponibles para toda la aplicaci�n.
    Driver driver;
    private JTextField AddDataTypeText1;
    private JTextField AddDataTypeText2;
    private JTextField AddDataNameText1;
    private JTextField AddDataNameText2;
    private JTextField RelationText1;
    private JTextField RelationText2;
    private JTextField RelationNameText;
    private JTextField AnyCommandText;
    
    private JButton AddDataButton;
    private JButton ShowDataButton;
    private JButton DeleteDataButton;
    private JButton DeleteSpecificDataButton;
    private JButton DeleteNodeRelation;
    private JButton DeleteRelation;
    private JButton MakeRelationButton;
    private JButton AddDataProperty;
    private JButton AnyCommandButton;
    
    private JPanel contentPane;

    public MainNeo(String uri, String user, String password)
    {
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password));
	    setTitle("WIKIFUTBOL");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 345);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(6, 6, 6, 6));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		//A�adimos la posicion de los botones
		
		AddDataTypeText1 = new JTextField();
		AddDataTypeText1.setBounds(10, 48, 200, 20);
		
		AddDataTypeText2 = new JTextField();
		AddDataTypeText2.setBounds(10, 68, 200, 20);
		
		AddDataNameText1 = new JTextField();
		AddDataNameText1.setBounds(10, 88, 200, 20);
		
		AddDataNameText2 = new JTextField();
		AddDataNameText2.setBounds(10, 108, 200, 20);
		
		RelationText1 = new JTextField();
		RelationText1.setBounds(10, 148, 200, 20);
		
		RelationText2 = new JTextField();
		RelationText2.setBounds(10, 168, 200, 20);
		
		RelationNameText = new JTextField();
		RelationNameText.setBounds(220, 168, 200, 20);
		
		
		AnyCommandText = new JTextField();
		AnyCommandText.setBounds(10,208,410,20);
		

		
		
		
		
		contentPane.add(AddDataTypeText1);
		contentPane.add(AddDataTypeText2);
		contentPane.add(AddDataNameText1);
		contentPane.add(AddDataNameText2);
		contentPane.add(RelationText1);
		contentPane.add(RelationText2);
		contentPane.add(RelationNameText);
		contentPane.add(AnyCommandText);
		
		
		AddDataTypeText1.setColumns(10);
		AddDataTypeText2.setColumns(10);
		AddDataNameText1.setColumns(10);
		AddDataNameText2.setColumns(10);
		RelationText1.setColumns(10);
		RelationText2.setColumns(10);
		RelationNameText.setColumns(10);
		AnyCommandText.setColumns(10);
		
		
		AddDataButton=new JButton("A�adir Equipo");
		AddDataButton.addActionListener(new ActionListener() {
			//Crear/a�adir cualquier tipo de datos
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Has creado/usado el  nodo "+AddDataNameText2.getText()+" de tipo "+AddDataTypeText1.getText());
		        try (Session session = driver.session())
		        {
		            session.writeTransaction(tx -> tx.run("MERGE (a:"+AddDataTypeText1.getText()+"{"+AddDataNameText1.getText()+": $x})", parameters("x",RelationText1.getText())));
		        }
			}
		});
		AddDataButton.setBounds(10, 10, 200, 20);
		contentPane.add(AddDataButton);
		
		ShowDataButton=new JButton("Mostrar Equipos");
		ShowDataButton.addActionListener(new ActionListener() {
			//Mostrar cualquier dato
			public void actionPerformed(ActionEvent arg0) {
				 try ( Session session = driver.session() ) {
			            List<String> greeting = session.writeTransaction( new TransactionWork<List<String>>() {

			                public List<String> execute( Transaction tx ) {
			                    Result result = tx.run( "MATCH (x:"+AddDataTypeText1.getText()+") " +
                                        "RETURN x."+AddDataNameText1.getText());
			                    return result.stream().map(elem -> elem.get(elem.size() - 1).toString()).collect(Collectors.toList());
			                }
			            } );
			            System.out.println(greeting.toString());
			        }
		    }
		});
		ShowDataButton.setBounds(220, 10, 200, 20);
		contentPane.add(ShowDataButton);
		
		DeleteDataButton=new JButton("Borrar Equipo");
		DeleteDataButton.addActionListener(new ActionListener() {
			//Borrar cualquier dato/nodo
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Has borrado el  nodo  de tipo "+AddDataTypeText1.getText());
		        try (Session session = driver.session())
		        {
		            session.writeTransaction(tx -> tx.run("MATCH (n:"+AddDataTypeText1.getText()+") DELETE n"));
		        }
		    }
		});
		DeleteDataButton.setBounds(220, 48, 200, 20);
		contentPane.add(DeleteDataButton);
		
		
		DeleteSpecificDataButton=new JButton("Borrar Equipo Especifico");
		DeleteSpecificDataButton.addActionListener(new ActionListener() {
			//Borrar cualquier dato/nodo
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Has borrado el  nodo "+AddDataNameText1.getText()+" de tipo "+AddDataTypeText1.getText());
		        try (Session session = driver.session())
		        {
		            session.writeTransaction(tx -> tx.run("MATCH (a:"+AddDataTypeText1.getText()+" { "+AddDataNameText1.getText()+" : '"+RelationText1.getText()+"' }) DELETE a"));
		        }
		    }
		});
		DeleteSpecificDataButton.setBounds(220, 68, 200, 20);
		contentPane.add(DeleteSpecificDataButton);
		
		
		MakeRelationButton=new JButton("Hacer relacion");
		MakeRelationButton.addActionListener(new ActionListener() {
			//Hacer una relacion 
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Has relacionado a "+RelationText1.getText()+" con "+RelationText2.getText()+" como "+RelationNameText.getText());
		        try (Session session = driver.session())
		        {
		            session.writeTransaction(tx -> tx.run
("MATCH (a:"+AddDataTypeText1.getText()+"{"+AddDataNameText1.getText()+":'"+RelationText1.getText()+"'}),(b:"+AddDataTypeText2.getText()+"{"+AddDataNameText2.getText()+":'"+RelationText2.getText()+"'}) MERGE (a)-[r:"+RelationNameText.getText()+"]->(b)"));
		        }
		    }
		});
		MakeRelationButton.setBounds(220, 88, 200, 20);
		contentPane.add(MakeRelationButton);
		
		
		
		AnyCommandButton=new JButton("Comando");
		AnyCommandButton.addActionListener(new ActionListener() {
			//Delete Any type of data/person
			public void actionPerformed(ActionEvent arg0) {
		        try (Session session = driver.session())
		        {
		            session.writeTransaction(tx -> tx.run(AnyCommandText.getText()));
		        }
		    }
		});
		AnyCommandButton.setBounds(220, 108, 200, 20);
		contentPane.add(AnyCommandButton);
		
		
		DeleteNodeRelation=new JButton("BorrarRelacionEquipos");
		DeleteNodeRelation.addActionListener(new ActionListener() {
			//Borrar cualquier relacion entre nodos
			public void actionPerformed(ActionEvent arg0) {
		        try (Session session = driver.session())
		        {
		            session.writeTransaction(tx -> tx.run("MATCH (n:"+AddDataTypeText1.getText()+" {"+AddDataNameText1.getText()+": '"+RelationText1.getText()+"'})" + "DETACH DELETE n"));
		        }
		    }
		});
		DeleteNodeRelation.setBounds(220, 128, 200, 20);
		contentPane.add(DeleteNodeRelation);
		
		
		DeleteRelation=new JButton("BorrarRelacion");
		DeleteRelation.addActionListener(new ActionListener() {
			//Borrar relacion
			public void actionPerformed(ActionEvent arg0) {
		        try (Session session = driver.session())
		        {
		            session.writeTransaction(tx -> tx.run("MATCH (n {"+AddDataNameText1.getText()+": '"+RelationText1.getText()+"'})-[r:"+RelationNameText.getText()+"]->()DELETE r"));
		        }
		    }
		});
		DeleteRelation.setBounds(220, 148, 200, 20);
		contentPane.add(DeleteRelation);
		
		
		AddDataProperty=new JButton("A�adir Propiedad");
		AddDataProperty.addActionListener(new ActionListener() {
			//A�adir una propiedad
			public void actionPerformed(ActionEvent arg0) {
		        try (Session session = driver.session())
		        {
		            session.writeTransaction(tx -> tx.run("MATCH (n {"+AddDataNameText1.getText()+": '"+RelationText1.getText()+"'}) SET n."+AddDataNameText2.getText()+"='"+RelationText2.getText()+"'"));
		        }
		    }
		});
		AddDataProperty.setBounds(220, 248, 200, 20);
		contentPane.add(AddDataProperty);
		
        
        setVisible(true);
    }


    public void close()
    {
        // Closing a driver immediately shuts down all open connections.
        driver.close();
    }

    public static void main(String... args)
    {
        MainNeo example = new MainNeo("bolt://localhost:7687", "neo4j", "1234");
    }
}



